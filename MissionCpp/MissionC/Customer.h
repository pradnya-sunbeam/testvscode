#include<string>
using namespace std;

#ifndef CUSTOMER_H
#define CUSTOMER_H
#include"ItemList.h"
#include"Date.h"

class customer
{
	string name;
	int tableNo;
	foodItemLinkList customerOrderList;
	Date BillDate;
public:
	customer();
	foodItemLinkList* getCustomerOrderList(foodItemLinkList* l);
	void printCustomerBill();

};

#endif