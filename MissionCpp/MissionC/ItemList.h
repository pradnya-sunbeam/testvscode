#include<string>
using namespace std;
#ifndef FOODLIST_H
#define FOODLIST_H
class foodItem
{
	string foodName;
	float price;
	int quantity;
public:
	 void acceptOnefoodNamePrice();
	 void addOneItemQuantity();
	 void printItemNamePrice();
	 void printItem();
	 float getPrice();
	 string getFoodName();
	 int getQuantity();
	 void setQuantity(int q);


};
class foodItemLinkList;
class foodItemNode
{
	foodItem data;
	foodItemNode* next;
	foodItemNode* priv;
	friend foodItemLinkList;
public:
	foodItemNode();
	foodItemNode(foodItem _data);  //create node code
};
class foodItemLinkList
{
	foodItemNode* head;
	int flag_cookedStatus; //0->uncooked    1-> cooked
public:
	foodItemLinkList();
	void AddLast(foodItem data);
	void deleteFirst();
	void ReadFromFile(char filename[25]);
	void saveItemsInFile(char filename[25]);
	foodItem* FindFoodItem(string name);
	void ChangeCookingStatus(int status);
	void PrintItemLinkList(int flag);
	void FreeList();
	float CalculateAmount();

};

#endif