#include<string>
#include<iostream>
using namespace std;
#include"ItemList.h"
//----------------------------------foodItemLinkList function implementation

//#define MENU_CARD_FILE "E:\\CourseEndProject\\C\\MissionC\\MenuCard.data"
foodItemLinkList::foodItemLinkList()
{
	head = NULL;
	flag_cookedStatus = 0;
}
void foodItemLinkList::AddLast(foodItem data)
{
	foodItemNode* ptr_new_Node = new foodItemNode();
//	ptr_new_Node->data.acceptOnefoodNamePrice();
	ptr_new_Node->data = data;

			//addfirst
	if (this->head == NULL)
	{
		this->head = ptr_new_Node;
	}
	else
	{
		if (this->head->next == NULL)
		{
			head->next = ptr_new_Node;
			ptr_new_Node->priv = head;
		}
		else
		{	//addlast
			//foodItemNode *temp;
			foodItemNode *trav = this->head;
			while (trav->next != NULL)
			{
				//temp = trav;
				trav = trav->next;
			}
			trav->next = ptr_new_Node;
			ptr_new_Node = trav;
		}
	}
}
float foodItemLinkList::CalculateAmount()
{
	float amount=0.0f;
	if (this->head == NULL)
	{
		cout << "\nlist is empty";
	}
	else
	{	//addlast
		foodItemNode *trav = this->head;
		while (trav != NULL)
		{
			amount += trav->data.getPrice()*trav->data.getQuantity();
			trav = trav->next;
		}
	}
	return amount;
}
void foodItemLinkList::PrintItemLinkList(int flag) //4->NamePrice only 5->printall
{
	if (this->head == NULL)
	{
		cout << "\nlist is empty";
	}
	else
	{	//addlast
		foodItemNode *trav = this->head;
		while (trav!= NULL)
		{
			if (flag == 4)
				trav->data.printItemNamePrice();
			if (flag == 5)
				trav->data.printItem();
			trav = trav->next;
		}
	}
}
void foodItemLinkList::deleteFirst()
{
	if (this->head == NULL)
		printf("LinkList is empty");
	else
	{
		if (this->head->next == NULL)
		{
			delete this->head;
			this->head = NULL;
		}
		else
		{
			foodItemNode* temp = this->head;
			this->head = this->head->next;
			head->priv = NULL;
			delete temp;
		}
	}
}
void foodItemLinkList::ReadFromFile(char filename[25])
{
	//FILE *fp = fopen(MENU_CARD_FILE, "rb+");
	FILE *fp = fopen("E:\\CourseEndProject\\C\\MissionC\\MenuCard.data", "rb+");
	//fseek(fp, 0, SEEK_END);
	foodItem food;
	if (fp != NULL)
	{
		while (fread(&food, sizeof(foodItem), 1, fp))
		{
			this->AddLast(food);
		}
		fclose(fp);
	}
}
void foodItemLinkList::saveItemsInFile(char filename[25])
{
	if (this->head == NULL)
	{
		cout << "\nlist is empty";
	}
	else
	{	//addlast
		//FILE *fp = fopen(MENU_CARD_FILE, "wb+");
		FILE *fp = fopen(filename, "wb+");
		foodItemNode *trav = this->head;
		while (trav != NULL)
		{
			fwrite(&trav->data, sizeof(foodItem), 1, fp);
			trav = trav->next;
		}
		fclose(fp);
	}
}
foodItem* foodItemLinkList::FindFoodItem(string name)
{
	foodItem *returnItem=NULL;
	if (this->head == NULL)
	{
		cout << "\nlist is empty";
		return NULL;
	}
	else
	{	//addlast
		foodItemNode *trav = this->head;
		while (trav != NULL)
		{
			if (trav->data.getFoodName() == name)
			{
				return &trav->data;
			}
			trav = trav->next;
		}
	}
	return returnItem;
}
void foodItemLinkList::ChangeCookingStatus(int status)
{
	this->flag_cookedStatus = status;
}
void foodItemLinkList::FreeList()
{

}
//----------------------------------foodItemNode function implementation
foodItemNode::foodItemNode(foodItem _data)
{
	this->data = _data;
	this->next = NULL;
	this->priv = NULL;
}
foodItemNode::foodItemNode()
{
	this->next = NULL;
	this->priv = NULL;
}
//---------------------------------foodItem function implementation
void foodItem::acceptOnefoodNamePrice()
{
	cout << "Enter food name";
	cin >> this->foodName;
	cout << "Enter Price";
	cin >> this->price;
}
void foodItem::addOneItemQuantity()
{
	cout << "Enter quantity";
	cin >> this->quantity;
}
void foodItem::printItemNamePrice()
{
	cout << "\nfood name="<<foodName<<"\tPrice="<<price;
}
void foodItem::printItem()
{
	cout << "\nfood name=" << foodName  << "\tquantity=" << quantity <<"\tPrice=" << price;
}
float foodItem::getPrice()
{
	return this->price;
}
string foodItem::getFoodName()
{
	return this->foodName;
}
int foodItem::getQuantity()
{
	return this->quantity;
}
void foodItem::setQuantity(int q)
{
	this->quantity = q;
}